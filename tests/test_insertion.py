import pytest
import src.app.sort_base as sort
import random


def test_corr_elem_order_ascending():
    """
    GIVEN array of numbers
    WHEN the bubble_sort method is called with ascending option
    THEN return sorted array
    """
    test = [random.randint(-100, 100) for _ in range(20)]
    ideal = sorted(test)
    bubble = sort.insert_sort(test)
    assert ideal == list(bubble)


def test_corr_elem_order_descending():
    """
    GIVEN array of numbers
    WHEN the bubble_sort method is called with reverse option
    THEN return reversed sorted array
    """
    test = [random.randint(-100, 100) for _ in range(20)]
    ideal = list(reversed(sorted(test)))
    bubble = sort.insert_sort(test, reverse=True)
    assert ideal == list(bubble)


def test_corr_input_data():
    """
    GIVEN input array
    WHEN it's filled with incorrect data (not numbers)
    THEN class must raise exception
    """
    test = [1, 2, 3.41125, '3.14', 'abc']
    with pytest.raises(TypeError):
        list(sort.insert_sort(test))


def test_stability_ascending():
    """
    GIVEN array of tuples of two numbers
    WHEN the bubble_sort method is called
    THEN return array with correctly placed tuples (read stable sorts)
    """
    test = [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]
    ideal = sorted(test)
    bubble = sort.insert_sort(test)
    assert ideal == list(bubble)


def test_stability_descending():
    """
    GIVEN array of tuples of two numbers (descending)
    WHEN the bubble_sort method is called
    THEN return array with correctly placed tuples (read stable sorts)
    """
    test = [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]
    ideal = list(reversed(sorted(test)))
    bubble = sort.insert_sort(test, reverse=True)
    assert ideal == list(bubble)
