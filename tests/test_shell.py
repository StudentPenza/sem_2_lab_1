import pytest
import src.app.sort_base as sort
import random


def test_ascending():

    test_lst = [random.randint(-100, 100) for _ in range(20)]
    ideal = sorted(test_lst)
    bubble = sort.shell_sort(test_lst)
    assert ideal == list(bubble)


def test_descending():

    test_lst = [random.randint(-100, 100) for _ in range(20)]
    ideal = list(reversed(sorted(test_lst)))
    bubble = sort.shell_sort(test_lst, reverse=True)
    assert ideal == list(bubble)


def test_input_data():

    test_lst = [1, 2, 3.41125, '3.14', 'abc']
    with pytest.raises(TypeError):
        list(sort.shell_sort(test_lst))


def test_stability_ascending():

    test_lst = [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]
    ideal = sorted(test_lst)
    bubble = sort.shell_sort(test_lst)
    assert ideal == list(bubble)


def test_stability_descending():

    test_lst = [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]
    ideal = list(reversed(sorted(test_lst)))
    bubble = sort.shell_sort(test_lst, reverse=True)
    assert ideal == list(bubble)
