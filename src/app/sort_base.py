def bubble_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        swapped = True
        if reverse:
            compare = lambda first, second: first < second
        else:
            compare = lambda first, second: first > second
        while swapped:
            swapped = False
            for i in range(len(lst) - 1):
                if compare(lst[i], lst[i + 1]):
                    lst[i], lst[i + 1] = lst[i + 1], lst[i]
                    swapped = True
        return lst


def shell_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        if reverse:
            compare = lambda first, second: first < second
        else:
            compare = lambda first, second: first > second
        t = len(lst) // 2
        while t > 0:
            for i in range(len(lst) - t):
                j = i
                while j >= 0 and compare(lst[j], lst[j + t]):
                    lst[j], lst[j + t] = lst[j + t], lst[j]
                    j -= 1
            t = t // 2
        return lst


def insert_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        if reverse:
            compare = lambda first, second: first < second
        else:
            compare = lambda first, second: first > second
        for i in range(1, len(lst)):
            key = lst[i]
            j = i - 1
            while j >= 0 and compare(lst[j], key):
                lst[j + 1] = lst[j]
                j -= 1
            lst[j + 1] = key
        return lst


def quicksort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        pivot = lst[len(lst)//2]
    l_nums = [n for n in lst if n < pivot]
    e_nums = [pivot] * lst.count(pivot)
    b_nums = [n for n in lst if n > pivot]
    lst = quicksort(l_nums) + e_nums + quicksort(b_nums)
    if reverse:
        return lst[::-1]
    else:
        return lst
